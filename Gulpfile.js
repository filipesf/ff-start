/* ==========================================================================
   GULPFILE
   ========================================================================== */

var
  gulp = require('gulp'),
  del = require('del'),
  critical = require('critical').stream,
  sassdoc = require('sassdoc'),
  gutil = require('gulp-util'),
  ftp = require('vinyl-ftp'),
  inc = require('gulp-file-include'),
  bs = require('browser-sync').create(),
  md = require('gulp-github-style-page'),
  rs = require('run-sequence'),
  $ = require('gulp-load-plugins')();

/**
 * Default paths
 */
var config = {
  stylesPath: 'source/styles',
  scriptsPath: 'source/scripts',
  imagesPath: 'source/images',
  viewsPath: 'source/views',
  buildPath: 'build'
};



/* CLEAR
   ========================================================================== */
gulp.task('clear', function() {
  return del(
    config.buildPath + '/**/*', { force: true }
  );
});




/* STYLES
   ========================================================================== */
gulp.task('styles', function() {
  return gulp.src(config.stylesPath + '/**/*.scss')
    .pipe($.sass({ outputStyle: 'compressed' }))
    .pipe(gulp.dest(config.buildPath + '/css'))
    .pipe($.size({ title: 'Styles' }))
    .pipe(bs.stream());
});



/* SASSDOC
   ========================================================================== */
gulp.task('sassdoc', function() {
  return gulp.src(config.stylesPath + '/**/*')
    .pipe(sassdoc({ dest: config.buildPath + '/docs' }))
    .pipe($.size({ title: 'SassDoc' }))
    .pipe(bs.stream());
});



/* SCRIPTS
   ========================================================================== */
gulp.task('scripts', function() {
  return gulp.src([
      config.scriptsPath + '/**/*.js',
      './node_modules/angular/angular.min.js'
    ])
    .pipe($.uglify())
    .pipe(gulp.dest(config.buildPath + '/js'))
    .pipe($.size({ title: 'Scripts' }));
});



/* IMAGES
   ========================================================================== */
gulp.task('images', function() {
  return gulp.src(config.imagesPath + '/**/*')
    .pipe($.imagemin())
    .pipe(gulp.dest(config.buildPath + '/img'))
    .pipe($.size({ title: 'Images' }))
    .pipe(bs.stream());
});



/* VIEWS
   ========================================================================== */
gulp.task('views', function() {
  return gulp.src(config.viewsPath + '/*.html')
    .pipe(md({
      customizeTemplatePath: config.viewsPath + '/_layouts/default.html'
    }).on('error', $.notify.onError()))
    .pipe(inc({
      basepath: config.viewsPath + '/_includes'
    }).on('error', $.notify.onError()))
    .pipe($.cacheBust({ type: 'timestamp' }))
    .pipe(gulp.dest(config.buildPath))
    .pipe(bs.stream());
});



/* CRITICAL
   ========================================================================== */
gulp.task('critical', function() {
  return gulp.src(config.buildPath + '/*.html')
    .pipe(critical({
      base: config.buildPath,
      inline: true,
      minify: true
    }).on('error', $.notify.onError()))
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      minifyCSS: true,
      minifyJS: true
    }).on('error', $.notify.onError()))
    .pipe(gulp.dest(config.buildPath));
});



/* BUILD
   ========================================================================== */
gulp.task('build', function(cb) {
  rs(
    'clear',
    'styles',
    'scripts',
    'images',
    'views',
    'critical',
    'sassdoc',
    cb
  );
});



/* WATCH
   ========================================================================== */
gulp.task('watch', function() {
  gulp.watch(config.scriptsPath + '/**/*', ['scripts']);
  gulp.watch(config.stylesPath + '/**/*', ['styles']);
  gulp.watch(config.imagesPath + '/**/*', ['images']);
  gulp.watch(config.viewsPath + '/**/*', ['views']);
});



/* SERVE
   ========================================================================== */
gulp.task('serve', function() {
  bs.init({
    server: {
      baseDir: config.buildPath,
      port: 3000
    }
  });
});



/* FTP
   ========================================================================== */
gulp.task('ftp', function() {
  var conn = ftp.create({
    user: '',
    password: '',
    host: '',
    log: gutil.log
  });

  return gulp.src(config.buildPath + '/**')
    .pipe(conn.newer('/ff-start'))
    .pipe(conn.dest('/ff-start'));
});



/* DEPLOY
   ========================================================================== */
gulp.task('deploy', function(cb) {
  rs(
    'build',
    'ftp',
    cb
  );
});



/* DEFAULT
   ========================================================================== */
gulp.task('default', function(cb) {
  rs(
    'build',
    'watch',
    'serve',
    cb
  );
});
